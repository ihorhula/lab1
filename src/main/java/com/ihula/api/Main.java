package com.ihula.api;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.AdjacencyListGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.graph.implementations.SingleNode;
import org.graphstream.ui.graphicGraph.GraphicGraph;
import org.graphstream.ui.spriteManager.Sprite;
import org.graphstream.ui.spriteManager.SpriteManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.MatchResult;

/**
 * Created by ihula on 23.05.16.
 */
public class Main {

    public static Graph graph = new SingleGraph("graph");

    static public void main(String[] args) throws IOException {
        new Main();
    }

    public Main() throws IOException {
        G g = graph("/home/jcrm/test/lab1/src/main/resources/graph.txt");
        Kernighan k = Kernighan.apply(g);

        for (V v : k.getGroupA()) {
            graph.getNode(v.name).addAttribute("ui.style", "fill-color: red;");
        }

        System.out.print("Group A: ");
        for (V x : k.getGroupA())
            System.out.print(x);
        System.out.print("\nGroup B: ");
        for (V x : k.getGroupB())
            System.out.print(x);
        System.out.println("");
        System.out.println("Cut cost: "+k.getCutCost());
    }

    public static G graph(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        G g = fromReadable(bufferedReader);
        bufferedReader.close();

        return g;
    }

    public static G fromReadable(Readable readable) {
        G g = new G();
        SpriteManager sman = new SpriteManager(graph);

        HashMap<String, V> names = new HashMap<String, V>();

        Scanner s = new Scanner(readable);

        while(s.hasNext("\r|\n")) s.next("\r|\n");

        s.skip("vertices:");
        while (s.findInLine("([A-Z])") != null) {
            MatchResult match = s.match();

            String name = match.group(1);
            V v = new V(name);
            g.addVertex(v);
            graph.addNode(v.name);
            graph.getNode(v.name).addAttribute("ui.label", v.name);
            names.put(name, v);
        }

        s.skip("\nedges:");
        while (s.findInLine("([A-Z])([A-Z])\\(([0-9]+(?:\\.[0-9]+)?)\\)") != null) {
            MatchResult match = s.match();

            V first = names.get(match.group(1));
            V second = names.get(match.group(2));
            Double weight = Double.parseDouble(match.group(3));
            g.addEdge(new E(weight), first, second);
            graph.addEdge(first.name + second.name, first.name, second.name);
            graph.getEdge(first.name + second.name).addAttribute("ui.label", weight);
        }

        graph.display();
        return g;
    }
}
