package com.ihula.api;

import java.util.*;

/**
 * Created by ihula on 23.05.16.
 */
public class Kernighan {
    public static Kernighan apply(G g) {
        return new Kernighan(g);
    }

    public class VertexGroup extends HashSet<V> {
        public VertexGroup(HashSet<V> clone) { super(clone); }
        public VertexGroup() { }
    }

    final private VertexGroup A, B;
    final private VertexGroup unswappedA, unswappedB;
    public VertexGroup getGroupA() { return A; }
    public VertexGroup getGroupB() { return B; }

    final private G g;
    public G getG() { return g; }
    final private int partitionSize;

    private Kernighan(G g) {
        this.g = g;
        this.partitionSize = g.getVertices().size() / 2;

        if (g.getVertices().size() != partitionSize * 2)
            throw new RuntimeException("Size of vertices must be even");

        A = new VertexGroup();
        B = new VertexGroup();

        int i = 0;
        for (V v : g.getVertices()) {
            (++i > partitionSize ? B : A).add(v);
        }
        unswappedA = new VertexGroup(A);
        unswappedB = new VertexGroup(B);

        System.out.println(A.size()+" "+B.size());

        doAllSwaps();
    }

    private void doAllSwaps() {

        LinkedList<Pair<V>> swaps = new LinkedList<Pair<V>>();
        double minCost = Double.POSITIVE_INFINITY;
        int minId = -1;

        for (int i = 0; i < partitionSize; i++) {
            double cost = doSingleSwap(swaps);
            if (cost < minCost) {
                minCost = cost; minId = i;
            }
        }

        while (swaps.size()-1 > minId) {
            Pair<V> pair = swaps.pop();
            swapVertices(A, pair.second, B, pair.first);
        }
    }

    private double doSingleSwap(Deque<Pair<V>> swaps) {

        Pair<V> maxPair = null;
        double maxGain = Double.NEGATIVE_INFINITY;

        for (V v_a : unswappedA) {
            for (V v_b : unswappedB) {

                E e = g.findEdge(v_a, v_b);
                double edge_cost = (e != null) ? e.weight : 0;
                double gain = getVertexCost(v_a) + getVertexCost(v_b) - 2 * edge_cost;

                if (gain > maxGain) {
                    maxPair = new Pair<V>(v_a, v_b);
                    maxGain = gain;
                }

            }
        }

        swapVertices(A, maxPair.first, B, maxPair.second);
        swaps.push(maxPair);
        unswappedA.remove(maxPair.first);
        unswappedB.remove(maxPair.second);

        return getCutCost();
    }

    private double getVertexCost(V v) {

        double cost = 0;

        boolean v1isInA = A.contains(v);

        for (V v2 : g.getNeighbors(v)) {

            boolean v2isInA = A.contains(v2);
            E e = g.findEdge(v, v2);

            if (v1isInA != v2isInA) // external
                cost += e.weight;
            else
                cost -= e.weight;
        }
        return cost;
    }

    public double getCutCost() {
        double cost = 0;

        for (E e : g.getEdges()) {
            Pair<V> endpoints = g.getEndpoints(e);

            boolean firstInA = A.contains(endpoints.first);
            boolean secondInA= A.contains(endpoints.second);

            if (firstInA != secondInA)
                cost += e.weight;
        }
        return cost;
    }

    private static void swapVertices(VertexGroup a, V va, VertexGroup b, V vb) {
        if (!a.contains(va) || a.contains(vb) ||
                !b.contains(vb) || b.contains(va)) throw new RuntimeException("Invalid swap");
        a.remove(va); a.add(vb);
        b.remove(vb); b.add(va);
    }
}

class E {
    public final double weight;

    public E(double weight) {
        this.weight = weight;
    }
}

class V {
    public final String name;

    public V(String name) {
        this.name = name;

    }

    @Override
    public String toString() {
        return name;
    }

}

class Pair<T> {
    final public T first;
    final public T second;

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }
}

class G {
    final private Map<V, Map<V, E>> vertices;
    final private Map<E, Pair<V>> edges;

    public G() {
        vertices = new HashMap<V, Map<V, E>>();
        edges = new HashMap<E, Pair<V>>();
    }

    public boolean addVertex(V v) {
        if (containsVertex(v)) return false;
        vertices.put(v, new HashMap<V, E>());
        return true;
    }

    public boolean addEdge(E e, V v1, V v2) {

        if (!containsVertex(v1) || !containsVertex(v2)) return false;
        if (findEdge(v1, v2) != null) return false;

        Pair<V> pair = new Pair<V>(v1, v2);
        edges.put(e, pair);
        vertices.get(v1).put(v2, e);
        vertices.get(v2).put(v1, e);

        return true;
    }

    public boolean containsVertex(V v) {
        return vertices.containsKey(v);
    }

    public E findEdge(V v1, V v2) {
        if (!containsVertex(v1) || !containsVertex(v2))
            return null;
        return vertices.get(v1).get(v2);
    }

    public Collection<V> getNeighbors(V v) {
        if (!containsVertex(v)) return null;
        return vertices.get(v).keySet();
    }

    public Set<E> getEdges() {
        return edges.keySet();
    }

    public Set<V> getVertices() {
        return vertices.keySet();
    }

    public Pair<V> getEndpoints(E e) {
        return edges.get(e);
    }

}
